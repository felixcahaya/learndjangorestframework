from __future__ import unicode_literals

from django.apps import AppConfig


class DjangoRestAppConfig(AppConfig):
    name = 'django_rest_app'
