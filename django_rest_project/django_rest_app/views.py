from rest_framework import status
from rest_framework.decorators import APIView
from rest_framework.response import Response
from rest_framework import generics
from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from django.http import Http404
from django.contrib.auth.models import User
from django_rest_app.models import Snippet
from django_rest_app.serializers import SnippetSerializer, UserSerial
from django_rest_app.permissions import IsOwnerOrReadOnly

# Create your views here.


class SnippetList(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    def get(self, request):
        snippets = Snippet.objects.all()
        s = SnippetSerializer(snippets, many=True)
        return Response(s.data, status=status.HTTP_200_OK, template_name=None, headers={"status": "berhasil"})

    def post(self, request):
        s = SnippetSerializer(data=request.data)
        if s.is_valid():
            s.save(owner=request.user)
            return Response(s.data, status=status.HTTP_201_CREATED)
        return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)


class SnippetDetail(APIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    def get_object(self, pk):
        try:
            return Snippet.objects.get(pk=pk)
        except Snippet.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        snippet = self.get_object(pk)
        s = SnippetSerializer(snippet)
        return Response(s.data)

    def post(self, request):
        snippet = self.get_object(pk)
        s = SnippetSerializer(snippet, data=request.data)
        if s.is_valid():
            s.save()
            return Response(s.data)
        return Response(s.errors, status=HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status.HTTP_204_NO_CONTENT)


class UserList(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    queryset = User.objects.all()
    serializer_class = UserSerial

class UserDetail(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    queryset = User.objects.all()
    serializer_class = UserSerial
