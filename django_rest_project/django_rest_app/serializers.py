from django.contrib.auth.models import User
from rest_framework import serializers
from django_rest_app.models import Snippet

class SnippetSerializer(serializers.ModelSerializer):
	owner = serializers.ReadOnlyField(source='owner.username')

	class Meta:
		model = Snippet
		fields = ('id', 'title', 'code', 'linenos', 'owner')

class UserSerial(serializers.ModelSerializer):
	snippets = serializers.PrimaryKeyRelatedField(many=True, queryset=Snippet.objects.all())

	class Meta:
		model = User
		fields = ('id', 'username', 'snippets')
		depth = 1