from __future__ import unicode_literals

from django.db import models

# Create your models here.
LANGUAGE_CHOICES = ("Python", "PHP", "Ruby")
STYLE_CHOICES = ("friendly", "unfriendly")

class Snippet(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default='')
    code = models.TextField()
    linenos = models.BooleanField(default=False)
    owner = models.ForeignKey('auth.User', related_name='snippets', on_delete=models.CASCADE)

    class Meta:
        ordering = ('created',)